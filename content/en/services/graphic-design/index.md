---
title: "Graphic Design"
date: 2023-08-05T16:18:16-06:00
draft: false
description: "Here we explain the benefits of creating your website with us."
services: ['Graphics Design']
showTableOfContents: true
showSummary: true
summary: Elevate your brand with our exceptional graphic designs
---

{{< lead >}}
## Elevate your brand with our exceptional graphic designs.
{{< /lead >}}

At Carima Digital, our graphics portfolio speaks about the beautiful and high quality work we deliver. We take great pride in the amazing talent of our creative steward, Tsiru.

When you choose to work with us, you're not just hiring a graphic designer; you're collaborating with a seasoned artist who pours her heart into every project. Our passion for artistry sets us apart, and our expertise allows us to approach each design uniquely, making sure it aligns perfectly with your brand and vision.

## Our Graphic Design offerings
- Logo design
- Brand design and brand strategy consultation
- Social Media marketing campaigns
- Custom graphic design: flyers, product labels, printed material, stickers.

## About our Creative Director
Tsiru (María Bustamante), our Creative Director, is an eclectic multi-faceted artist with years of experience and uniquely talented as graphics designer, painter and handcrafter. Drawing inspiration from the Costa Rican jungle and rural life, her art transcends ordinary expression. Tsiru brings a touch of magic to our Graphic Design and Web Design services, elevating your projects with exceptional creativity and expertise.

Check out Tsiru's Instagram, to catch an expression of her talent in painted art:

{{< icon "instagram" >}} [tsiru.art](https://instagram.com/tsiru.art) 

## Taster of graphic design portfolio

{{< gallery >}}
	<img src="img/carima_presencia_digital_logo.png" class="grid-w33" />
	<img src="img/mujer_creciente_logo_logo_blanco.png" class="grid-w33" />
	<img src="img/Gania_Waldorf_Education_fondo_blanco.png" class="grid-w33" />
	<img src="img/LogoTsiru.jpg" class="grid-w33" />
	<img src="img/HB_LOGO.jpg" class="grid-w33" />
	<img src="img/Bamschool_logo_blanco.png" class="grid-w33" />
	<img src="img/logo_chocaloha_color_1.jpg" class="grid-w33" />
	<img src="img/flyer_bri_propuesta3_ingles_cambios.jpg" class="grid-w33" />
	<img src="img/etiketa_gingerbeer2.jpg" class="grid-w33" />
	<img src="img/rioafiche_gratiferia_abril.jpg" class="grid-w33" />
	<img src="img/ternateaArmoniaVeg.jpg" class="grid-w33" />
	<img src="img/logo_escobar.jpg" class="grid-w33" />
	<img src="img/triptico1_regenesis.jpg" class="grid-w33" />
	<img src="img/ChocAloha.png" class="grid-w33" />
	<img src="img/bloom3.png" class="grid-w33" />
	<img src="img/afiche_refi_2023_print.png" class="grid-w33" />
{{< /gallery >}}